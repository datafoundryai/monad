import React from "react";
import Badges from "./Badges";

export default {
  title: "Badges",
  component: Badges,
};

export const Primary = () => <Badges badgeType="normal">Primary</Badges>;
export const Danger = () => <Badges badgeType="danger">Danger</Badges>;
export const Success = () => <Badges badgeType="success">Success</Badges>;
export const SuccessPill = () => (
  <Badges pill badgeType="success">
    Success Pill
  </Badges>
);
export const DangerPill = () => (
  <Badges pill badgeType="danger">
    Danger Pill
  </Badges>
);
export const PrimaryPill = () => (
  <Badges pill badgeType="normal">
    Primary Pill
  </Badges>
);
