import React from "react";
import Badge from "react-bootstrap/Badge";
import "bootstrap/dist/css/bootstrap.css";
import "./Badges.scss";
import PropTypes from "prop-types";

function Badges(props) {
  const { badgeType, pill, children, ...rest } = props;
  return (
    <div>
      {pill ? (
        <Badge pill className={`badge ${badgeType}`} {...rest}>
          {children}
        </Badge>
      ) : (
        <Badge className={`badge ${badgeType}`} {...rest}>
          {children}
        </Badge>
      )}
    </div>
  );
}

Badges.propTypes = {
  badgeType: PropTypes.string,
  rest: PropTypes.string,
  pill: PropTypes.string,
  children: PropTypes.string,
};

Badges.defaultProps = {
  badgeType: null,
  pill: null,
  children: null,
  rest: null,
};
export default Badges;
