import React from "react";
import "./Button.scss";
import PropTypes from "prop-types";

function Button(props) {
  const { variant, children, ...rest } = props;
  return (
    <div className="btn-con">
      <button type="submit" className={`button ${variant}`} {...rest}>
        {children}
      </button>
    </div>
  );
}

Button.propTypes = {
  variant: PropTypes.string,
  children: PropTypes.string,
};

Button.defaultProps = {
  variant: null,
  children: null,
};

export default Button;
