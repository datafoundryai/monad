import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

export { default as Button } from "./components/Button/Button";
export { default as Badges } from "./components/Badges/Badges";
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
